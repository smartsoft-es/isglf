#
# Copyright (C) 2013 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

define Profile/Streamnow_ISG
	NAME:=Streamnow ISG
	PACKAGES:=\
		uboot-sunxi-Streamnow_ISG kmod-rtc-sunxi swconfig
endef

define Profile/Streamnow_ISG/Description
	Package set optimized for the Streamnow ISG
endef

$(eval $(call Profile,Streamnow_ISG))
